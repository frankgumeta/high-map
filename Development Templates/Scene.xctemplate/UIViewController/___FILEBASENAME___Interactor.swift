//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ___VARIABLE_sceneName___InteractorInterface {
    
}

class ___VARIABLE_sceneName___Interactor {
    var presenter: ___VARIABLE_sceneName___PresenterInterface?
}

extension ___VARIABLE_sceneName___Interactor: ___VARIABLE_sceneName___InteractorInterface {
    
}
