//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ___VARIABLE_sceneName___PresenterInterface {
    
}

class ___VARIABLE_sceneName___Presenter {
    weak var controller: ___VARIABLE_sceneName___ViewControllerInterface?
  
}

extension  ___VARIABLE_sceneName___Presenter: ___VARIABLE_sceneName___PresenterInterface {
    
}
