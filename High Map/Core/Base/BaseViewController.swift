//
//  BaseViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 22/03/21.
//

import UIKit

protocol BackButtonable {
    func setupBackButton()
}

extension BackButtonable where Self: BaseViewController {
    func setupBackButton() {
        let backButton = UIBarButtonItem(title: "Back",
                                         style: UIBarButtonItem.Style.plain,
                                         target: self,
                                         action: #selector(BaseViewController.dismiss as (BaseViewController) -> () -> ()))
        self.navigationItem.leftBarButtonItem = backButton
    }
}

protocol CleanCompliant {
    func setupCleanArcStack()
}

class BaseViewController: UIViewController, CleanCompliant, BackButtonable {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCleanArcStack()
    }
    
    func setupCleanArcStack() { }
}

extension BaseViewController {
    @IBAction func dismiss() {
        dismiss(animated: true)
    }
}
 
