//
//  Alertable.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//

import UIKit

protocol Alertable {}

extension Alertable where Self: UIViewController {
    
    func showAlert(_ msg: String, action: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: "High Map", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: action)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    
    func showGeneralError() {
        showAlert("Ocurrio un error, intente de nuevo en un momento.")
    }
}
