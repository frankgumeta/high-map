//
//  Loadable.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 01/07/21.
//

import UIKit

protocol Loadable {}

extension Loadable where Self: UIViewController {
    func startLoader(activityColor: UIColor = .white,
                     backgroundColor: UIColor = .black) {
            
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 5
        backgroundView.alpha = 0.7
            
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.medium
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
            
        backgroundView.addSubview(activityIndicator)

        self.view.addSubview(backgroundView)
    }

    func stopLoader() {
        if let background = view.viewWithTag(5) {
            background.removeFromSuperview()
        }
        self.view.isUserInteractionEnabled = true
    }
}

