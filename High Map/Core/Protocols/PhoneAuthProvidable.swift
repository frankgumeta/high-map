//
//  PhoneAuthProvidable.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//

import Foundation
import FirebaseAuth
import Firebase
import FBSDKLoginKit

protocol AuthProvidable: PhoneAuthProvidable, AppleAuthProvidable {}

extension AuthProvidable {
    func logOut(isLoggedOut: @escaping(Bool) -> Void) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            let loginManager = LoginManager()
            loginManager.logOut()
            isLoggedOut(true)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            isLoggedOut(false)
        }
    }
}

protocol PhoneAuthProvidable {
    func sendAuthSMS(_ phone: String, uiDelegate: AuthUIDelegate, id: @escaping(String) -> Void)
    func verifyAuthSMS(verificationID: String, codeText: String, userData: @escaping(AuthDataResult?) -> Void)
}

extension PhoneAuthProvidable {
    func sendAuthSMS(_ phone: String, uiDelegate: AuthUIDelegate, id: @escaping(String) -> Void) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: uiDelegate) { (verificationID, error) in
            id(verificationID ?? "")
        }
    }
    
    func verifyAuthSMS(verificationID: String, codeText: String, userData: @escaping(AuthDataResult?) -> Void) {
        let credential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: codeText)
        Auth.auth().signIn(with: credential) { (user, error) in
            userData(user)
        }
    }
}
