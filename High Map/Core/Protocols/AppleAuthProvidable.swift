//
//  AppleAuthProvidable.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 15/04/21.
//

import AuthenticationServices
import FirebaseAuth
import Firebase
import CryptoKit

protocol AppleAuthProvidable {
    func startSignInWithAppleFlow(currentNonce: String, delegate: ASAuthorizationControllerDelegate & ASAuthorizationControllerPresentationContextProviding)
}

extension AppleAuthProvidable {

    @available(iOS 13, *)
    func startSignInWithAppleFlow(currentNonce: String, delegate: ASAuthorizationControllerDelegate & ASAuthorizationControllerPresentationContextProviding) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(currentNonce)

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = delegate
        authorizationController.presentationContextProvider = delegate
        authorizationController.performRequests()
    }

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
          return String(format: "%02x", $0)
        }.joined()

        return hashString
    }
}
