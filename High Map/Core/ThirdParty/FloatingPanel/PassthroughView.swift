// Copyright 2018-Present Shin Yamamoto. All rights reserved. MIT license.

import UIKit

//swiftlint:disable lower_acl_than_parent
@objc(FloatingPanelPassthroughView)
class PassthroughView: UIView {
    public weak var eventForwardingView: UIView?
    override public func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        switch hitView {
        case self:
            return eventForwardingView?.hitTest(self.convert(point, to: eventForwardingView), with: event)
        default:
            return hitView
        }
    }
}
