//
//  Product.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 02/06/21.
//

import Foundation

struct Product: Codable {
    var name: String?
    var tags: [String?]
    var type: String?
    var imageURL: String?
    var rating: Double?
    var description: String?
    var isFavorite: Bool?
}
