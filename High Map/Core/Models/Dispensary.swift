//
//  Dispensary.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 02/06/21.
//

import Foundation

struct Dispensary: Codable {
    var firebaseKey: String?
    var name: String?
    var productType: [String]?
    var address: String?
    var location: Location?
    var imageURL: String?
    var rating: Double?
    var openAt: String?
    var closingAt: String?
    var products: [Product]?
    var phoneNumber: String?
    var storeType: String?
}

// MARK: - Location
struct Location: Codable {
    let latitude, longitude: Double?
}
