//
//  DataService.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 01/06/21.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()

class DataService {
    static let instance = DataService()
    
    private init() {}
    
    private var _REF_BASE = DB_BASE
    private var _REF_DISPENSARIES = DB_BASE.child("dispensaries")
    private var _REF_BUYERS = DB_BASE.child("buyers")
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_DISPENSARIES: DatabaseReference {
        return _REF_DISPENSARIES
    }
    
    var REF_BUYERS: DatabaseReference {
        return _REF_BUYERS
    }
    
    
    func createFirebaseDBUser(uid: String, userData: Dictionary<String, Any>, isBuyer: Bool) {
        if isBuyer {
            REF_BUYERS.child(uid).updateChildValues(userData)
        } else {
            REF_DISPENSARIES.child(uid).updateChildValues(userData)
        }
    }
    
    func sendChatMessage(_ channel: String, _ uid: String, _ message: Dictionary<String, Any>) {
        REF_DISPENSARIES.child(uid).child("Chats").child(channel).childByAutoId().updateChildValues(message)
    }
    
    func getDispensaries(_ response: @escaping([Dispensary]) -> Void) {
        DataService.instance.REF_DISPENSARIES.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot] {
                var dispensaries = [Dispensary]()
                for dispensary in snapshot {
                    guard let data = try? JSONSerialization.data(withJSONObject: dispensary.value as Any, options: []) else { return }
                    guard var object = try? JSONDecoder().decode(Dispensary.self, from: data) else { return }
                    object.firebaseKey = dispensary.key
                    dispensaries.append(object)
                }
                response(dispensaries)
            }
        })
    }
    
    func createMockDispensary() {
        
        let mockProduct: Dictionary<String, Any>
        mockProduct = [
            "name": "Weed Sativa",
            "type": "Flowers",
            "description": "Hierba anual, con tallo erecto, estriado longitudinalmente, que puede alcanzar hasta los 3 m de altura. Hojas alternas, palmaticompuestas, con 5-7 foliolos lanceolados, acuminados, de margen serrado. Dioica, las flores tienen perianto monoclamídeo, y son poco llamativas. Las masculinas se agrupan en panículas laxas; las femeninas en glomérulos compactos en la axila de las brácteas superiores. Toda la planta tiene abundantes pelos simples y, en la región de la inflorescencia femenina, también abundantes pelos secretores (que producen la resina conocida como hachís).",
            "tags": ["CBD", "Flowers", "THC"],
            "imageURL": "https://cms-assets.tutsplus.com/uploads/users/1631/posts/34758/image/Weed%20Logo%20Maker%20for%20a%20Cannabis%20Dispensary%20copy.jpg",
            "rating": 4.1,
            "isFavorite": true
        ]
        
        let data: Dictionary<String, Any>
        data = [
            "name": "Weed Store",
            "productType": ["Edibles", "Flowers", "Sativa"],
            "address": "San Francisco #2092, Zapopan, Jalisco",
            "location": ["latitude": 20.71252215636825, "longitude": -103.41262687592773],
            "imageURL": "https://cms-assets.tutsplus.com/uploads/users/1631/posts/34758/image/Weed%20Logo%20Maker%20for%20a%20Cannabis%20Dispensary%20copy.jpg",
            "rating": 4.1,
            "openAt": "9am",
            "closingAt": "6pm",
            "products": [
                mockProduct, mockProduct, mockProduct, mockProduct
            ],
            "phoneNumber": "6442062522",
            "storeType": "Dispensario"
        ]
        REF_DISPENSARIES.childByAutoId().updateChildValues(data)
    }
}

