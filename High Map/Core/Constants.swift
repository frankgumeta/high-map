//
//  Constants.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 23/03/21.
//

import Foundation
import UIKit

enum UIConstants {
    static let defaultCornerRadius: CGFloat = 8.0
    static let defaultBorderWidth: CGFloat = 1.0
}
