//
//  UIView+Extensions.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 23/03/21.
//

import Foundation
import UIKit

extension UIView {
    func roundCorners(radius: CGFloat = UIConstants.defaultCornerRadius) {
        clipsToBounds = true
        layer.cornerRadius = radius
    }
    
    func addShadow() {
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 4
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        layer.masksToBounds = false
    }
    
    func addBorder(width: CGFloat = UIConstants.defaultBorderWidth, color: UIColor = UIColor.black) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    func fadeTo(alphaValue: CGFloat, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.alpha = alphaValue
        }
    }
}
