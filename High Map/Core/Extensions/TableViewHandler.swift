//
//  TableViewHandler.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 01/07/21.
//

import UIKit

class TableViewHandler<Cell: UITableViewCell, Element> : NSObject, UITableViewDataSource, UITableViewDelegate {
    
    typealias TableViewDataSourceCellConfigurationBlock = (Cell, Element) -> ()
    typealias TableViewDataSourceCellSelectorHandler = (Element) -> ()
    
    // MARK: - Internal Properties
    
    private(set) var cellConfigurationBlock: TableViewDataSourceCellConfigurationBlock
    private(set) var cellSelectorHandler: TableViewDataSourceCellSelectorHandler
    
    // MARK: - Private Properties
    
    private let cellIdentifier: String
    private var items: [Element]
    
    // MARK: - Initializers
    
    init(_ cellIdentifier: String,_ items: [Element] , cellConfigurationBlock: @escaping TableViewDataSourceCellConfigurationBlock, cellSelectorHandler: @escaping TableViewDataSourceCellSelectorHandler) {
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.cellConfigurationBlock = cellConfigurationBlock
        self.cellSelectorHandler = cellSelectorHandler
    }
    
    // MARK: - DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! Cell
        let item = items[indexPath.row]
        self.cellConfigurationBlock(cell, item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        self.cellSelectorHandler(item)
    }
    
}

