//
//  UIImageView+Extensions.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 01/07/21.
//

import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
    
    func load(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        load(url: url)
    }
}
