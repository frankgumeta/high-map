//
//  UIColor+Extensions.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//

import Foundation
import UIKit

extension UIColor {
    // MARK: - Background
    static var background0: UIColor { return UIColor(named: "Background0") ?? UIColor.red }
    static var background1: UIColor { return UIColor(named: "Background1") ?? UIColor.red }
    
    static var primary0: UIColor { return UIColor(named: "Primary0") ?? UIColor.red }
    static var primary1: UIColor { return UIColor(named: "Primary1") ?? UIColor.red }
    static var primary2: UIColor { return UIColor(named: "Primary2") ?? UIColor.red }
    static var primary3: UIColor { return UIColor(named: "Primary3") ?? UIColor.red }
    static var primary4: UIColor { return UIColor(named: "Primary4") ?? UIColor.red }
}
