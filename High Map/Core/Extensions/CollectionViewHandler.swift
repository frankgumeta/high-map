//
//  CollectionViewHandler.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 01/07/21.
//

import UIKit

protocol CollectionConfigurable {
    var cellSize: CGSize { get }
    var insets: UIEdgeInsets { get }
}

struct CollectionConfig: CollectionConfigurable {
    var cellSize: CGSize
    var insets: UIEdgeInsets
    var scrollDirection: UICollectionView.ScrollDirection
    
    init(cellSize: CGSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 180),
         insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
         scrollDirection: UICollectionView.ScrollDirection = .vertical) {
        self.cellSize = cellSize
        self.insets = insets
        self.scrollDirection = scrollDirection
    }
}

class CollectionViewHandler<Cell: UICollectionViewCell, Element>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    typealias CollectionDataSourceCellConfigurationBlock = (Cell, Element) -> ()
    typealias CollectionDataSourceCellSelectorHandler = (Element) -> ()
    
    // MARK: - Internal Properties
    
    private(set) var cellConfigurationBlock: CollectionDataSourceCellConfigurationBlock
    private(set) var cellSelectorHandler: CollectionDataSourceCellSelectorHandler
    
    // MARK: - Private Properties
    private let cellIdentifier: String
    private var items: [Element]
    private var config: CollectionConfig
    
    // MARK: - Initializers
    
    init(collection: UICollectionView,
         cellIdentifier: String,
         items: [Element],
         cellConfigurationBlock: @escaping CollectionDataSourceCellConfigurationBlock,
         cellSelectorHandler: @escaping CollectionDataSourceCellSelectorHandler,
         config: CollectionConfig = CollectionConfig()) {
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.cellConfigurationBlock = cellConfigurationBlock
        self.cellSelectorHandler = cellSelectorHandler
        self.config = config
        if let flowLayout = collection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = config.scrollDirection
        }
    }
    
    // MARK: - DataSource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! Cell
        let item = items[indexPath.row]
        self.cellConfigurationBlock(cell, item)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        self.cellSelectorHandler(item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return config.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return config.insets
    }
}


