//
//  Cells+Extensions.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 07/04/21.
//

import Foundation

protocol Configurable {
    static func identifier() -> String
}

extension Configurable {
    static func identifier() -> String {
        return String(describing: self)
    }
}
