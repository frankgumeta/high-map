//
//  UIViewController+Extensions.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//

import UIKit

extension UIViewController {
    func presentLoadingView(_ status: Bool) {
        var fadeView: UIView?
        let screenSize = UIScreen.main.bounds
        
        if status == true {
            fadeView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
            fadeView?.backgroundColor = UIColor.black
            fadeView?.alpha = 0.0
            fadeView?.tag = 99
            
            let spinner = UIActivityIndicatorView()
            spinner.color = UIColor.white
            spinner.style = UIActivityIndicatorView.Style.large
            spinner.center = CGPoint(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY)
            
            view.addSubview(fadeView!)
            fadeView?.addSubview(spinner)
            
            spinner.startAnimating()
            
            fadeView?.fadeTo(alphaValue: 0.7, withDuration: 0.2)
        } else {
            for subview in view.subviews {
                if subview.tag == 99 {
                    UIView.animate(withDuration: 0.2, animations: {
                        subview.alpha = 0.0
                    }, completion: { (finished) in
                        subview.removeFromSuperview()
                    })
                }
            }
        }
    }
}
