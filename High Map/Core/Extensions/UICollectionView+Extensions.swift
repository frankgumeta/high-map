//
//  UICollectionView+Extensions.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 07/04/21.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerNib(with cell: Configurable.Type) {
        let nib = UINib(nibName: cell.identifier(), bundle: nil)
        register(nib, forCellWithReuseIdentifier: cell.identifier())
    }
}
