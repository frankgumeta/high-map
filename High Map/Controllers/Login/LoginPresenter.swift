//
//  LoginPresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 23/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol LoginPresenterInterface {
    var controller: LoginViewControllerInterface? { get }
    func showAlert(_ msg: String)
    func passVerificationScreen(_ verificationID: String,_ phone: String)
    func getSignInStatus(authResult: AuthDataResult?, error: Error?)
}

class LoginPresenter {
    weak var controller: LoginViewControllerInterface?
  
}

extension LoginPresenter: LoginPresenterInterface {
    
    func showAlert(_ msg: String) {
        controller?.presentAlert(msg)
    }
    
    func passVerificationScreen(_ verificationID: String,_ phone: String) {
        controller?.presentVerificationScreen(verificationID, phone)
    }
    
    func getSignInStatus(authResult: AuthDataResult?, error: Error?) {
        guard let error = error else {
            controller?.presentMainScreen()
            return
        }
        controller?.presentAlert(error.localizedDescription)
    }
}
