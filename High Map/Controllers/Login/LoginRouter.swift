//
//  LoginRouter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 23/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class LoginRouter {
    weak var controller: LoginViewControllerInterface?
    weak var routingDelegate: LandingRoutingDelegate?
    
    func goToMainScreen() {
        (controller as? UIViewController)?.dismiss(animated: true, completion: { [weak self] in
            self?.routingDelegate?.goToMainScreen()
        })
    }

    private weak var navigator: UIViewController? {
        return (controller as? LoginViewController)
    }
    
    func goToSMSVerification(_ verificationID: String,_ phone: String) {
        let smsController = SMSVerificationViewController() { [weak self] isVerified in
            if isVerified {
                self?.goToMainScreen()
            }
        }
        smsController.interactor?.verificationID = verificationID
        smsController.interactor?.phoneNumber = phone
        let navigation = UINavigationController(rootViewController: smsController)
        navigator?.present(navigation, animated: true)
    }
}
