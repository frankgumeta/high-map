//
//  LoginViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 23/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import AuthenticationServices
import FBSDKLoginKit
import FirebaseAuth

protocol LoginViewControllerInterface: AuthUIDelegate {
    func presentAlert(_ msg: String)
    func presentVerificationScreen(_ verificationID: String,_ phone: String)
    func presentMainScreen()
}

class LoginViewController: BaseViewController {
    
    private var interactor: LoginInteractorInterface?
    private var router: LoginRouter?
    
    // MARK: - IBOutlets
    @IBOutlet private weak var phoneTextfield: UITextField!
    @IBOutlet private weak var fieldsContainer: UIView! {
        didSet {
            fieldsContainer.roundCorners()
            fieldsContainer.addShadow()
        }
    }
    @IBOutlet private weak var loginButton: BaseUIButton!
    @IBOutlet private weak var appleLoginButtonContainer: UIView! {
        didSet {
            appleLoginButtonContainer.roundCorners()
            appleLoginButton = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .black)
            guard let appleButton = appleLoginButton else {
                return
            }
            appleButton.frame = appleLoginButtonContainer.bounds
            appleLoginButtonContainer.addSubview(appleButton)
        }
    }
    @IBOutlet private weak var facebookLoginButtonContainer: UIView! {
        didSet {
            facebookLoginButtonContainer.roundCorners()
            facebookLoginButton = FBLoginButton()
            facebookLoginButton?.delegate = self
            facebookLoginButton?.permissions = ["email","public_profile"]
            guard let facebookButton = facebookLoginButton else {
                return
            }
            facebookLoginButton?.frame = facebookLoginButtonContainer.bounds
            facebookLoginButtonContainer.addSubview(facebookButton)
        }
    }
    
    private var facebookLoginButton: FBLoginButton?
    private var appleLoginButton : ASAuthorizationAppleIDButton? {
        didSet {
            appleLoginButton?.addTarget(self, action: #selector(appleButtonDidPressed), for: .touchUpInside)
        }
    }
    private weak var routingDelegate: LandingRoutingDelegate?
    
    // MARK: - Init
    convenience init() {
        self.init(routingDelegate: nil)
    }
    
    init(routingDelegate: LandingRoutingDelegate?) {
        self.routingDelegate = routingDelegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Draw Lifecycle
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        fieldsContainer.addShadow()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = LoginInteractor()
        let presenter = LoginPresenter()
        let router = LoginRouter()
        router.routingDelegate = routingDelegate
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
    
    // MARK: - Actions
    @IBAction private func goToMainScreen() {
        router?.goToMainScreen()
    }
}

extension LoginViewController {
    // MARK: - Actions
    @IBAction private func continueButtonDidPressed() {
        interactor?.loadSMSVerification(phone: phoneTextfield.text)
    }
    
    @objc
    private func appleButtonDidPressed() {
        guard let nonce = interactor?.currentNonce else { return }
        self.startSignInWithAppleFlow(currentNonce: nonce, delegate: self)
    }
}

extension LoginViewController: LoginViewControllerInterface, Alertable {
    func presentAlert(_ msg: String) {
        showAlert(msg)
    }
    
    func presentVerificationScreen(_ verificationID: String,_ phone: String) {
        router?.goToSMSVerification(verificationID, phone)
    }
    
    func presentMainScreen() {
        router?.goToMainScreen()
    }
}

// MARK: - AppleSign In Delegate

extension LoginViewController: AppleAuthProvidable, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        interactor?.checkAppleSignAuthStatus(authorization: authorization)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Sign in with Apple errored: \(error)")
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return ASPresentationAnchor(frame: self.view.frame)
    }
}

// MARK: - Facebook In Delegates

extension LoginViewController: LoginButtonDelegate {

    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        interactor?.checkFacebookSignAuthStatus(result: result, error: error)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        return
    }
}
