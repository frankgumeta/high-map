//
//  RegisterViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import AuthenticationServices
import FBSDKLoginKit
import FirebaseAuth

protocol RegisterViewControllerInterface: AuthUIDelegate {
    func presentAlert(_ msg: String)
    func presentVerificationScreen(_ verificationID: String,_ phone: String,_ mail: String)
    func presentMainScreen()
}

class RegisterViewController: BaseViewController {
  
    private var interactor: RegisterInteractorInterface?
    private var router: RegisterRouter?
    
    // MARK: - IBOutlets
    @IBOutlet private weak var emailTextfield: UITextField!
    @IBOutlet private weak var phoneTextfield: UITextField!
    @IBOutlet private weak var registerContainer: UIView! {
        didSet {
            registerContainer.roundCorners()
            registerContainer.addShadow()
        }
    }
    @IBOutlet private weak var facebookRegisterButtonContainer: UIView! {
        didSet {
            facebookRegisterButtonContainer.roundCorners()
            facebookLoginButton = FBLoginButton()
            facebookLoginButton?.delegate = self
            facebookLoginButton?.permissions = ["email","public_profile"]
            guard let facebookButton = facebookLoginButton else {
                return
            }
            facebookLoginButton?.frame = facebookRegisterButtonContainer.bounds
            facebookRegisterButtonContainer.addSubview(facebookButton)
        }
    }
    @IBOutlet private weak var appleRegisterButtonContainer: UIView! {
        didSet {
            appleRegisterButtonContainer.roundCorners()
            appleLoginButton = ASAuthorizationAppleIDButton(authorizationButtonType: .signIn, authorizationButtonStyle: .black)
            guard let appleButton = appleLoginButton else {
                return
            }
            appleButton.frame = appleRegisterButtonContainer.bounds
            appleRegisterButtonContainer.addSubview(appleButton)
        }
    }
    
    private var facebookLoginButton: FBLoginButton?
    private var appleLoginButton : ASAuthorizationAppleIDButton? {
        didSet {
            appleLoginButton?.addTarget(self, action: #selector(appleButtonDidPressed), for: .touchUpInside)
        }
    }
    private weak var routingDelegate: LandingRoutingDelegate?
    
    // MARK: - Init
    
    convenience init() {
        self.init(routingDelegate: nil)
    }
    
    init(routingDelegate: LandingRoutingDelegate?) {
        self.routingDelegate = routingDelegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = RegisterInteractor()
        let presenter = RegisterPresenter()
        let router = RegisterRouter()
        router.routingDelegate = routingDelegate
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
    
    // MARK: - Actions
    @IBAction private func createAccountButtonDidPressed() {
        interactor?.loadSMSVerification(phone: phoneTextfield.text, email: emailTextfield.text)
    }
}

extension RegisterViewController: AuthProvidable {
    
    @objc
    private func appleButtonDidPressed() {
        guard let nonce = interactor?.currentNonce else { return }
        self.startSignInWithAppleFlow(currentNonce: nonce, delegate: self)
    }
}

extension RegisterViewController: RegisterViewControllerInterface, Alertable {
    
    func presentAlert(_ msg: String) {
        showAlert(msg)
    }
    
    func presentMainScreen() {
        router?.goToMainScreen()
    }
    
    func presentVerificationScreen(_ verificationID: String, _ phone: String, _ mail: String) {
        router?.goToSMSVerification(verificationID, phone, mail)
    }
}

// MARK: - AppleSign In Delegate

extension RegisterViewController: AppleAuthProvidable, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        interactor?.checkAppleSignAuthStatus(authorization: authorization)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Sign in with Apple errored: \(error)")
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return ASPresentationAnchor(frame: self.view.frame)
    }
}

// MARK: - Facebook In Delegates

extension RegisterViewController: LoginButtonDelegate {

    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        interactor?.checkFacebookSignAuthStatus(result: result, error: error)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        return
    }
}

