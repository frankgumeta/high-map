//
//  RegisterPresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol RegisterPresenterInterface {
    var controller: RegisterViewControllerInterface? { get }
    func showAlert(_ msg: String)
    func getSignInStatus(authResult: AuthDataResult?, error: Error?)
    func passVerificationScreen(_ verificationID: String,_ phone: String,_ mail: String)
}

class RegisterPresenter {
    weak var controller: RegisterViewControllerInterface?
  
}

extension RegisterPresenter: RegisterPresenterInterface {
    
    func showAlert(_ msg: String) {
        controller?.presentAlert(msg)
    }
    
    func getSignInStatus(authResult: AuthDataResult?, error: Error?) {
        guard let error = error else {
            controller?.presentMainScreen()
            return
        }
        controller?.presentAlert(error.localizedDescription)
    }
    
    func passVerificationScreen(_ verificationID: String, _ phone: String, _ mail: String) {
        controller?.presentVerificationScreen(verificationID, phone, mail)
    }
}
