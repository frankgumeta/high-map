//
//  RegisterInteractor.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth
import AuthenticationServices
import FBSDKLoginKit

protocol RegisterInteractorInterface {
    var currentNonce: String? { get set }
    func loadSMSVerification(phone: String?, email: String?)
    func checkAppleSignAuthStatus(authorization: ASAuthorization)
    func checkFacebookSignAuthStatus(result: LoginManagerLoginResult?, error: Error?)
}

class RegisterInteractor {
    var presenter: RegisterPresenterInterface?
    var currentNonce: String? = "".randomNonceString()
    
    enum Constants {
        static let mxCode = "+52"
        static let emptyPhoneMailMsg = "Porfavor ingrese un email y/o número de teléfono válido"
        static let generalError = "Ocurrio un error, intente de nuevo en un momento."
    }
}

extension RegisterInteractor: RegisterInteractorInterface, AuthProvidable {
    
    func loadSMSVerification(phone: String?, email: String?) {
        if let phone = phone, phone.count == 10, let mail = email, mail.isEmail {
            guard let controller = presenter?.controller else {
                return
            }
            sendAuthSMS(Constants.mxCode + phone, uiDelegate: controller) { [weak self] verificationID in
                if !verificationID.isEmpty {
                    self?.presenter?.passVerificationScreen(verificationID, Constants.mxCode + phone, mail)
                    return
                }
                self?.presenter?.showAlert(Constants.generalError)
            }
        } else {
            presenter?.showAlert(Constants.emptyPhoneMailMsg)
        }
    }
    
    func checkAppleSignAuthStatus(authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
              fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
              print("Unable to fetch identity token")
              return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
              print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
              return
            }
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            Auth.auth().signIn(with: credential) { [weak self] (authResult, error) in
                if (error != nil) {
                    print(error?.localizedDescription ?? "")
                    return
                }
                self?.presenter?.getSignInStatus(authResult: authResult, error: error)
            }
        }
    }
    
    func checkFacebookSignAuthStatus(result: LoginManagerLoginResult?, error: Error?) {
        if let error = error {
            presenter?.showAlert(error.localizedDescription)
            return
        }
        guard let tokenString = AccessToken.current?.tokenString else {
            return
        }
        let credential = FacebookAuthProvider.credential(withAccessToken: tokenString)
        Auth.auth().signIn(with: credential) { [weak self] (authResult, error) in
            if let error = error {
                self?.presenter?.showAlert(error.localizedDescription)
                return
            }
            self?.presenter?.getSignInStatus(authResult: authResult, error: error)
        }
    }
}
