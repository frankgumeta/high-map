//
//  SMSVerificationPresenter.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol SMSVerificationPresentationLogic {
    func getAuthData(userData: AuthDataResult?)
}

class SMSVerificationPresenter: SMSVerificationPresentationLogic {
    
    weak var viewController: SMSVerificationDisplayLogic?
    
    func getAuthData(userData: AuthDataResult?) {
        viewController?.presentAuthData(userData: userData)
    }
}
