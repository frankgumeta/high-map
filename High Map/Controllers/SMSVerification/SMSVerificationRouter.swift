//
//  SMSVerificationRouter.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class SMSVerificationRouter {
  
    weak var viewController: SMSVerificationViewController?
    private weak var navigator: UINavigationController? {
        return viewController?.navigationController
    }
}
