//
//  SMSVerificationInteractor.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol SMSVerificationBusinessLogic {
    var verificationID: String? { get set }
    var phoneNumber: String? { get set }
    var email: String? { get set }
    func checkAuthSMS(verificationID: String, codeText: String)
}

protocol SMSVerificationDataStore {
}

class SMSVerificationInteractor: SMSVerificationBusinessLogic, SMSVerificationDataStore, AuthProvidable {
    var presenter: SMSVerificationPresentationLogic?
    var verificationID: String?
    var phoneNumber: String?
    var email: String?
    
    func checkAuthSMS(verificationID: String, codeText: String) {
        verifyAuthSMS(verificationID: verificationID, codeText: codeText) {[weak self] userData in
            if let email = self?.email, let user = Auth.auth().currentUser {
                user.updateEmail(to: email) { (error) in
                    print("email updated")
                }
            }
            self?.presenter?.getAuthData(userData: userData)
        }
    }
}
