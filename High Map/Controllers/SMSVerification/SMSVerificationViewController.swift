//
//  SMSVerificationViewController.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 29/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol SMSVerificationDisplayLogic: AnyObject {
    func presentAuthData(userData: AuthDataResult?)
}

class SMSVerificationViewController: UIViewController, Alertable {
    
    enum Constants {
        static let smsVerification = "Verificación SMS"
        static let next = "Siguiente"
    }
  
    var interactor: SMSVerificationBusinessLogic?
    var router: SMSVerificationRouter?
    private var isVerified: ((Bool) -> Void)?

    // MARK: IBOutlets
    
    @IBOutlet private weak var phoneNumberLabel: UILabel! {
        didSet {
            phoneNumberLabel.text = interactor?.phoneNumber?.toPhoneNumberFormat()
        }
    }
    @IBOutlet private weak var codeTextfield: UITextField!
    @IBOutlet private weak var resendCodeButton: UIButton!
    
    // MARK: - Init
    convenience init() {
        self.init(isVerified: nil)
    }
    
    init(isVerified: ((Bool) -> Void)?) {
        self.isVerified = isVerified
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
    // MARK: Setup
  
    private func setup() {
        let viewController = self
        let interactor = SMSVerificationInteractor()
        let presenter = SMSVerificationPresenter()
        let router = SMSVerificationRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarItems()
    }
    
    func setNavigationBarItems() {
        self.title = Constants.smsVerification
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = nil
        self.addNavigationItems()
    }
    
    func addNavigationItems() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.next, style: .plain, target: self, action: #selector(nextButtonPressed))
    }
    
    @objc
    func nextButtonPressed() {
        guard let id = interactor?.verificationID,
              let smsCode = codeTextfield.text,
              !smsCode.isEmpty else {
            showGeneralError()
            return
        }
        interactor?.checkAuthSMS(verificationID: id, codeText: smsCode)
    }
    
    // MARK: View Actions
    @IBAction func resendButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SMSVerificationViewController: SMSVerificationDisplayLogic {
    func presentAuthData(userData: AuthDataResult?) {
        guard let _ = userData else {
            showGeneralError()
            return
        }
        self.dismiss(animated: true) { [weak self] in
            self?.isVerified?(true)
        }
    }
}
