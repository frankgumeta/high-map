//
//  ProductsViewController.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 18/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ProductsViewControllerInterface: AnyObject {
    
}

class ProductsViewController: BaseViewController {
  
    private var interactor: ProductsInteractorInterface?
    private var router: ProductsRouter?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = ProductsInteractor()
        let presenter = ProductsPresenter()
        let router = ProductsRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension ProductsViewController {
    
}

extension ProductsViewController: ProductsViewControllerInterface {
    
}
