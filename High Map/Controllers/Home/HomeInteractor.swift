//
//  HomeInteractor.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeInteractorInterface {
    func getDispensaries()
}

class HomeInteractor {
    var presenter: HomePresenterInterface?
}

extension HomeInteractor: HomeInteractorInterface {
    
    func getDispensaries() {
        DataService.instance.getDispensaries { [weak self] response in
            self?.presenter?.getDispensaries(response)
        }
    }
}
