//
//  HomeStoreCollectionViewCell.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 06/04/21.
//

import UIKit

class HomeStoreCollectionViewCell: UICollectionViewCell, Configurable {
    
    @IBOutlet private weak var storeImageView: UIImageView! {
        didSet {
            storeImageView.roundCorners(radius: storeImageView.frame.size.height / 2)
            storeImageView.addBorder(width: 2, color: .background0)
        }
    }
    @IBOutlet private weak var storeNameLabel: UILabel!
    @IBOutlet private weak var storeContentLabel: UILabel!
    @IBOutlet private weak var storeDistanceLabel: UILabel!
    
    func setup(_ dispensary: Dispensary?) {
        guard let dispensary = dispensary else { return }
        self.storeNameLabel.text = dispensary.name ?? ""
        self.storeContentLabel.text = dispensary.productType?.joined(separator: ", ")
        self.storeDistanceLabel.text = "Latitude: \(dispensary.location?.latitude?.truncate(places: 2) ?? 0.0), Longitude: \(dispensary.location?.longitude?.truncate(places: 2) ?? 0.0)"
    }

}
