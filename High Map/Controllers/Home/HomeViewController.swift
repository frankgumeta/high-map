//
//  HomeViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomeViewControllerInterface: AnyObject {
    func presentDispensaries(_ response: [Dispensary])
}

class HomeViewController: BaseViewController, Loadable {
  
    private var interactor: HomeInteractorInterface?
    private var router: HomeRouter?
    
    // MARK: - IBOutlets
    @IBOutlet private weak var searchButton: UIButton! {
        didSet {
            searchButton.roundCorners()
        }
    }
    
    @IBOutlet private(set) weak var collectionView: UICollectionView!
    
    // MARK: - Variables
    
    private var datasource: HomeDataSource?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        startLoader()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor?.getDispensaries()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = HomeInteractor()
        let presenter = HomePresenter()
        let router = HomeRouter()
        
        datasource = HomeDataSource()
        datasource?.delegate = self
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension HomeViewController {
    private func setupCollectionView() {
        collectionView.registerNib(with: HomeStoreCollectionViewCell.self)
        collectionView.delegate = datasource
        collectionView.dataSource = datasource
    }
}

extension HomeViewController: HomeViewControllerInterface {
    
    func presentDispensaries(_ response: [Dispensary]) {
        datasource?.dispensaries = response
        collectionView.reloadData()
        stopLoader()
    }
}

extension HomeViewController: HomeDataSourceDelegate {
    func didSelect(_ dispensary: Dispensary) {
        router?.goToDetailDispensary(dispensary)
    }
}
