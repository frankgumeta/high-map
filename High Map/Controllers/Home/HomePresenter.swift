//
//  HomePresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol HomePresenterInterface {
    func getDispensaries(_ response: [Dispensary])
}

class HomePresenter {
    weak var controller: HomeViewControllerInterface?
  
}

extension  HomePresenter: HomePresenterInterface {
    
    func getDispensaries(_ response: [Dispensary]) {
        controller?.presentDispensaries(response)
    }
}
