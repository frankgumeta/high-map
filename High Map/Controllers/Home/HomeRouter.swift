//
//  HomeRouter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class HomeRouter {
    weak var controller: HomeViewControllerInterface?
    
    private weak var navigator: UIViewController? {
        return (controller as? HomeViewController)
    }
   
    
    func goToDetailDispensary(_ dispensary: Dispensary) {
        let dispensaryVC = DispensaryViewController()
        let navigation = UINavigationController(rootViewController: dispensaryVC)
        navigation.modalPresentationStyle = .fullScreen
        navigator?.present(navigation, animated: true, completion: {
            dispensaryVC.setupModel(dispensary)
        })
    }
}
