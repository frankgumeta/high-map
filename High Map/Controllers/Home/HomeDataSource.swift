//
//  HomeDataSource.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 06/04/21.
//

import Foundation
import UIKit

protocol HomeDataSourceDelegate: AnyObject {
    func didSelect(_ dispensary: Dispensary)
}

class HomeDataSource: NSObject {
    var dispensaries: [Dispensary]?
    weak var delegate: HomeDataSourceDelegate?
}

extension HomeDataSource: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selection = dispensaries?[indexPath.row] else { return }
        delegate?.didSelect(selection)
    }
}

extension HomeDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dispensaries?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeStoreCollectionViewCell", for: indexPath) as? HomeStoreCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setup(dispensaries?[indexPath.row])
        return cell
    }
}

extension HomeDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let height: CGFloat = 80.0
        
        return CGSize(width: width, height: height)
    }
    
}
