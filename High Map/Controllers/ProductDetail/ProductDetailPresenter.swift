//
//  ProductDetailPresenter.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 23/06/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ProductDetailPresenterInterface {
    
}

class ProductDetailPresenter {
    weak var controller: ProductDetailViewControllerInterface?
  
}

extension  ProductDetailPresenter: ProductDetailPresenterInterface {
    
}
