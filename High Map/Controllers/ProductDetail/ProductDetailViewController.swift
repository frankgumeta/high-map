//
//  ProductDetailViewController.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 23/06/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol ProductDetailViewControllerInterface: AnyObject {
    
}

class ProductDetailViewController: BaseViewController {
  
    private var interactor: ProductDetailInteractorInterface?
    private var router: ProductDetailRouter?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = ProductDetailInteractor()
        let presenter = ProductDetailPresenter()
        let router = ProductDetailRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension ProductDetailViewController {
    
}

extension ProductDetailViewController: ProductDetailViewControllerInterface {
    
}
