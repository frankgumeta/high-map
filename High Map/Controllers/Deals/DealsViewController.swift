//
//  DealsViewController.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 18/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DealsViewControllerInterface: AnyObject {
    
}

class DealsViewController: BaseViewController {
  
    private var interactor: DealsInteractorInterface?
    private var router: DealsRouter?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = DealsInteractor()
        let presenter = DealsPresenter()
        let router = DealsRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension DealsViewController {
    
}

extension DealsViewController: DealsViewControllerInterface {
    
}
