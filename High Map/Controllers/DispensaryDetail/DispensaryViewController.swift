//
//  DispensaryViewController.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 22/06/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DispensaryViewControllerInterface: AnyObject {
    func presentInitialData(viewModel: Dispensary)
}

class DispensaryViewController: BaseViewController, Loadable {
  
    // MARK: - Variables
    private var interactor: DispensaryInteractorInterface?
    private var router: DispensaryRouter?
    private var collectionHandler: CollectionViewHandler<ProductCell, Product>?
    private var collectionConfig: CollectionConfig?
    
    // MARK: - Outlets
    @IBOutlet private weak var storeImage: UIImageView!
    @IBOutlet private weak var collection: UICollectionView!
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = DispensaryInteractor()
        let presenter = DispensaryPresenter()
        let router = DispensaryRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
    
    func setupModel<T>(_ model: T) {
        if let model = model as? Dispensary {
            interactor?.viewModel = model
            setupInitialData()
        }
    }
    
    func setupCollection() {
        collection.register(UINib(nibName: "ProductCell",
                                  bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        collectionConfig = CollectionConfig(cellSize: CGSize(width: 150, height: 150), scrollDirection: .horizontal)
        collectionHandler = CollectionViewHandler<ProductCell, Product>(collection: collection,
                                                                        cellIdentifier: "ProductCell",
                                                                        items: interactor?.viewModel?.products ?? [], cellConfigurationBlock: { cell, product in
                                                                            cell.backgroundColor = .lightGray
                                                                            cell.setup(product: product)
                                                                        }, cellSelectorHandler: { product in
                                                                            print(product)
                                                                        }, config: collectionConfig ?? CollectionConfig())
        
        collection.delegate = collectionHandler
        collection.dataSource = collectionHandler
    }
}

private extension DispensaryViewController {
    func setupInitialData() {
        startLoader()
        interactor?.getInitialData()
    }
}

extension DispensaryViewController: DispensaryViewControllerInterface {

    func presentInitialData(viewModel: Dispensary) {
        title = viewModel.name ?? ""
        storeImage.load(urlString: viewModel.imageURL ?? "")
        setupCollection()
        collection.reloadData()
        stopLoader()
    }
}
