//
//  DispensaryPresenter.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 22/06/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DispensaryPresenterInterface {
    func getInitialData(viewModel: Dispensary)
}

class DispensaryPresenter {
    weak var controller: DispensaryViewControllerInterface?
  
}

extension  DispensaryPresenter: DispensaryPresenterInterface {
    
    func getInitialData(viewModel: Dispensary) {
        controller?.presentInitialData(viewModel: viewModel)
    }
}
