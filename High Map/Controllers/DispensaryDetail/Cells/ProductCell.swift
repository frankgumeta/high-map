//
//  ProductCell.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 08/07/21.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productNameLabel: UILabel!

    func setup(product: Product) {
        productNameLabel?.text = product.name
    }

}
