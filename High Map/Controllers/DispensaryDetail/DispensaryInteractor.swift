//
//  DispensaryInteractor.swift
//  High Map
//
//  Created by Roberto Gutierrez Gonzalez on 22/06/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DispensaryInteractorInterface {
    var viewModel: Dispensary? { get set }
    func getInitialData()
}

class DispensaryInteractor {
    var presenter: DispensaryPresenterInterface?
    var viewModel: Dispensary?
}

extension DispensaryInteractor: DispensaryInteractorInterface {
    
    func getInitialData() {
        guard let model = viewModel else { return }
        presenter?.getInitialData(viewModel: model)
    }
}

