//
//  UserProfileViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol UserProfileViewControllerInterface: AnyObject {
    func navigateToLanding()
}

class UserProfileViewController: BaseViewController {
  
    private var interactor: UserProfileInteractorInterface?
    private var router: UserProfileRouter?
    @IBOutlet private weak var logoutButton: BaseUIButton! {
        didSet {
            logoutButton?.addTarget(self, action: #selector(logoutButtonDidPressed), for: .touchUpInside)
        }
    }
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = UserProfileInteractor()
        let presenter = UserProfilePresenter()
        let router = UserProfileRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension UserProfileViewController {
    
    @objc
    private func logoutButtonDidPressed() {
        interactor?.logout()
    }
}

extension UserProfileViewController: UserProfileViewControllerInterface {
    func navigateToLanding() {
        router?.goToLandingPage()
    }
}
