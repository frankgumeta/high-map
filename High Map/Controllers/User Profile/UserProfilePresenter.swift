//
//  UserProfilePresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol UserProfilePresenterInterface {
    func presentLandingScreen()
}

class UserProfilePresenter {
    weak var controller: UserProfileViewControllerInterface?
  
}

extension  UserProfilePresenter: UserProfilePresenterInterface {
    func presentLandingScreen() {
        controller?.navigateToLanding()
    }
}
