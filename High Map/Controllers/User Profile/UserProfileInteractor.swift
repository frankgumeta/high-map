//
//  UserProfileInteractor.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol UserProfileInteractorInterface {
    func logout()
}

class UserProfileInteractor {
    var presenter: UserProfilePresenterInterface?
}

extension UserProfileInteractor: UserProfileInteractorInterface, AuthProvidable {
    func logout() {
        logOut { [weak self] success in
            if success { self?.presenter?.presentLandingScreen() }
        }
    }
}
