//
//  UserProfileRouter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 26/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class UserProfileRouter {
    weak var controller: UserProfileViewControllerInterface?
   
    private weak var navigator: UINavigationController? {
        return (controller as? UserProfileViewController)?.navigationController
    }
    
    func goToLandingPage() {
        if let landingPage = navigator?.viewControllers.first(where: { $0 is LandingViewController }) {
            navigator?.popToViewController(landingPage, animated: true)
        }
    }
}
