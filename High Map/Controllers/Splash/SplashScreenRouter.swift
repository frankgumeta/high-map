//
//  SplashScreenRouter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class SplashScreenRouter {
    weak var controller: SplashScreenViewControllerInterface?
    private weak var navigator: UINavigationController? {
        return (controller as? SplashScreenViewController)?.navigationController
    }
    
    func goToLandingPage() {
        let loginController = LandingViewController()
        navigator?.pushViewController(loginController, animated: true)
    }
}
