//
//  SplashScreenPresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenPresenterInterface {
    
}

class SplashScreenPresenter {
    weak var controller: SplashScreenViewControllerInterface?
  
}

extension  SplashScreenPresenter: SplashScreenPresenterInterface {
    
}
