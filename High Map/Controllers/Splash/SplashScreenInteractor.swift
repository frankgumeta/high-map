//
//  SplashScreenInteractor.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenInteractorInterface {
    
}

class SplashScreenInteractor {
    var presenter: SplashScreenPresenterInterface?
}

extension SplashScreenInteractor: SplashScreenInteractorInterface {
    
}
