//
//  SplashScreenViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 25/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenViewControllerInterface: AnyObject {
    
}

class SplashScreenViewController: BaseViewController {
  
    private var interactor: SplashScreenInteractorInterface?
    private var router: SplashScreenRouter?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router?.goToLandingPage()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = SplashScreenInteractor()
        let presenter = SplashScreenPresenter()
        let router = SplashScreenRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension SplashScreenViewController {
    
}

extension SplashScreenViewController: SplashScreenViewControllerInterface {
    
}
