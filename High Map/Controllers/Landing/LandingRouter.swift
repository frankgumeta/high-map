//
//  LandingRouter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 22/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import UIKit

class LandingRouter  {
    weak var controller: (LandingViewControllerInterface & LandingRoutingDelegate)?
    private weak var navigator: UINavigationController? {
        return (controller as? LandingViewController)?.navigationController
    }
   
    func goToLogin() {
        let loginController = LoginViewController(routingDelegate: controller)
        loginController.modalPresentationStyle = .overCurrentContext
        navigator?.present(loginController, animated: true)
    }
    
    func goToRegister() {
        let registerController = RegisterViewController(routingDelegate: controller)
        registerController.modalPresentationStyle = .overCurrentContext
        navigator?.present(registerController, animated: true)
    }
    
    func goToMainScreen() {
        let mainScreen = BaseTabBarController()
        
        let homeViewController = MapViewController()
        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), selectedImage: nil)
        mainScreen.addChild(homeViewController)
        
        let dealsViewController = DealsViewController()
        dealsViewController.tabBarItem = UITabBarItem(title: "Deals", image: UIImage(named: "deals"), selectedImage: nil)
        mainScreen.addChild(dealsViewController)
        
        let productsViewController = ProductsViewController()
        productsViewController.tabBarItem = UITabBarItem(title: "Products", image: UIImage(named: "marihuana"), selectedImage: nil)
        mainScreen.addChild(productsViewController)
        
        let strainsViewController = StrainsViewController()
        strainsViewController.tabBarItem = UITabBarItem(title: "Strains", image: UIImage(named: "joint"), selectedImage: nil)
        mainScreen.addChild(strainsViewController)
        
        let profileViewController = UserProfileViewController()
        profileViewController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: nil)
        mainScreen.addChild(profileViewController)
        
        navigator?.pushViewController(mainScreen, animated: true)
    }
}
