//
//  LandingPresenter.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 22/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol LandingPresenterInterface {
    func presentMainScreen()
}

class LandingPresenter {
    weak var controller: LandingViewControllerInterface?
  
}

extension  LandingPresenter: LandingPresenterInterface {
    func presentMainScreen() {
        controller?.presentMainScreen()
    }
}
