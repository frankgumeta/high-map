//
//  LandingViewController.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 22/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol LandingRoutingDelegate: AnyObject {
    func goToMainScreen()
}

protocol LandingViewControllerInterface: AnyObject {
    func presentMainScreen()
}

class LandingViewController: BaseViewController {
  
    private var interactor: LandingInteractorInterface?
    private var router: LandingRouter?
    
    @IBOutlet private weak var loginButton: BaseUIButton!
    @IBOutlet private weak var signIn: BaseUIButton!
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.checkIfUserIsLoggedIn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = LandingInteractor()
        let presenter = LandingPresenter()
        let router = LandingRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
    
    // MARK: - Actions
    @IBAction private func goToLogin() {
        router?.goToLogin()
    }
    
    @IBAction private func goToRegister() {
        router?.goToRegister()
    }
}

extension LandingViewController {
    
}

extension LandingViewController: LandingViewControllerInterface {
    func presentMainScreen() {
        goToMainScreen()
    }
}

extension LandingViewController: LandingRoutingDelegate {
    func goToMainScreen() {
        router?.goToMainScreen()
    }
}
