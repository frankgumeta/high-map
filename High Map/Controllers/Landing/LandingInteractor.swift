//
//  LandingInteractor.swift
//  High Map
//
//  Created by Francisco Eduardo Toledo Gumeta on 22/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol LandingInteractorInterface {
    func checkIfUserIsLoggedIn()
}

class LandingInteractor {
    var presenter: LandingPresenterInterface?
}

extension LandingInteractor: LandingInteractorInterface {
    
    func checkIfUserIsLoggedIn() {
        if Auth.auth().currentUser != nil {
            presenter?.presentMainScreen()
        }
    }
}
