//
//  StrainsInteractor.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 18/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol StrainsInteractorInterface {
    
}

class StrainsInteractor {
    var presenter: StrainsPresenterInterface?
}

extension StrainsInteractor: StrainsInteractorInterface {
    
}
