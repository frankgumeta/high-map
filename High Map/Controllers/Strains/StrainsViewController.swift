//
//  StrainsViewController.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 18/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol StrainsViewControllerInterface: AnyObject {
    
}

class StrainsViewController: BaseViewController {
  
    private var interactor: StrainsInteractorInterface?
    private var router: StrainsRouter?
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = StrainsInteractor()
        let presenter = StrainsPresenter()
        let router = StrainsRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
}

extension StrainsViewController {
    
}

extension StrainsViewController: StrainsViewControllerInterface {
    
}
