//
//  MapViewControllerInteractor.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 17/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MapInteractorInterface {
    
}

class MapInteractor {
    var presenter: MapPresenterInterface?
}

extension MapInteractor: MapInteractorInterface {
    
}
