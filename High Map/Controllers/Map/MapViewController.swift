//
//  MapViewControllerViewController.swift
//  High Map
//
//  Created by Gutierrez Gonzalez, Roberto (Cognizant) on 17/05/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol MapViewControllerViewControllerInterface: AnyObject {
    
}

class MapViewController: BaseViewController {
  
    private var interactor: MapInteractorInterface?
    private var router: MapRouter?
    private var floatingPanel: FloatingPanelController!
    private let locationManager = CLLocationManager()
    
    // MARK: - IBOutlets
    @IBOutlet private weak var mapView: MKMapView!
  
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFloatingView()
        setupMapView()
    }
    
    // MARK: - Setup
    override func setupCleanArcStack() {
        let controller = self
        let interactor = MapInteractor()
        let presenter = MapPresenter()
        let router = MapRouter()
        
        self.interactor = interactor
        controller.router = router
        interactor.presenter = presenter
        presenter.controller = controller
        router.controller = controller
    }
    
    // MARK: - Setup FloatingView
    func setupFloatingView() {
        floatingPanel = FloatingPanelController()
        floatingPanel.delegate = self
        let contentVC = HomeViewController()
        floatingPanel.set(contentViewController: contentVC)
        floatingPanel.track(scrollView: contentVC.collectionView)
        floatingPanel.addPanel(toParent: self)
    }
    
    // MARK: - Setup MapView
    func setupMapView() {
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

}

extension MapViewController {
    
}

extension MapViewController: MapViewControllerViewControllerInterface {
    
}

extension MapViewController: FloatingPanelControllerDelegate {
    
}


extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}
